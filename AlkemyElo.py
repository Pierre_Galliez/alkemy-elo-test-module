from tkinter import *
from tkinter import _setit
from tkinter import messagebox
from tkinter import ttk
from tkinter.simpledialog import askstring
from tkinter.messagebox import showinfo
import os
import math

#Variables globales
listeJoueur =["Joueur pair"] #.append pour ajouter
eloJoueurPair = 500
K = 20 #coefficient de d�veloppement, plus il est grand plus le elo varie
#Fin

def refreshListeJoueur():
    global labelListeJoueur
    global listeDeroulanteJoueur1
    global listeDeroulanteJoueur2
    labelListeJoueur.pack_forget()
    finalStr = ""
    for joueur in listeJoueur:
        if(joueur != "Joueur pair"):
            f = open("./data/"+joueur+".txt","r")
            elo = f.readline()
            f.close()
        else:
            elo = (str)(eloJoueurPair)
        finalStr += joueur + "\t" + elo + "\n"
    listeJoueur.sort()
    labelListeJoueur = Label(listeJoueurFrame, text=finalStr)
    labelListeJoueur.pack()
    listeDeroulanteJoueur1['values'] = listeJoueur
    listeDeroulanteJoueur2['values'] = listeJoueur

def importT3():
    global listeJoueur
    f = open("./import/import.csv","r")
    importString = []
    while True:
        line = f.readline()
        if not line:
            break
        importString.append(line)
    f.close()
    #On commence � 1 car 0 c'est les noms des champs, les donn�es commencent � 1
    labelString = ""
    for ligne in importString :
        spl = ligne.split(";") #Place;Pr�nom;Nom de famille;Pseudo;Arm�e;Origine;�quipe;Liste d'arm�e;Note de compo;Pay�
        if(spl[0] == "Place") :
            continue
        pseudo = spl[3]
        listeJoueur.append(pseudo)
        if(not os.path.exists("./data/"+pseudo+".txt")):
            elo = askstring("Ajout de joueur", "Quel elo de d�part pour "+pseudo+" ?")
            #showinfo("Ajout de joueur",'{}'.format(elo))
            f2 = open("./data/"+pseudo+".txt","w")
            f2.write(elo)
            f2.close()
    refreshListeJoueur()
        
def ParamK():
    global K
    K = askstring("Changer K","Nouvelle valeur de K (default = 20)")

def ajouterJoueur():
    global listeJoueur
    labelListeJoueur.pack_forget()
    pseudo = askstring("Ajout de joueur", "Pseudo du joueur � ajouter au tournoi :")
    listeJoueur.append(pseudo)
    if(not os.path.exists("./data/"+pseudo+".txt")):
            elo = askstring("Ajout de joueur", "Quel elo de d�part pour "+pseudo+" ?")
            #showinfo("Ajout de joueur",'{}'.format(elo))
            f2 = open("./data/"+pseudo+".txt","w")
            f2.write(elo)
            f2.close()
    finalStr = ""
    refreshListeJoueur()


def addBonusPoints():
    eloBonus = askstring("Points bonus", "Combien de points � ajouter aux joueurs ?")
    for joueur in listeJoueur :
        if(os.path.exists("./data/"+joueur+".txt")):
            f = open("./data/"+joueur+".txt","r")
            elo = f.readline()
            f.close()
            f = open("./data/"+joueur+".txt","w")
            f.write((str)((int)(eloBonus) + (int)(elo)))
            f.close()
    showinfo("Points Bonus","+"+eloBonus+" points ajout�s !")
    refreshListeJoueur()


def ParamJoueurPair():
    elo = askstring("Elo du joueur pair", "Quel elo de d�part pour le joueur pair ?")
    global eloJoueurPair
    eloJoueurPair = elo
    print("Elo du joueur pair : " +eloJoueurPair)
    refreshListeJoueur()


def ShowMatchFrame():
    matchFenetre.deiconify()

def QuitAll():
    matchFenetre.destroy()
    fenetre.destroy()

def validerMatch(j1, j2, egalite):
    if(os.path.exists("./data/"+j1+".txt")):
            f = open("./data/"+j1+".txt","r")
            elo1 = f.readline()
            f.close()
    elif(j1 == "Joueur pair"):
        elo1 = eloJoueurPair
        
    else:
        showinfo("Erreur", "Le joueur en haut n'a pas de fichier !")

    if(os.path.exists("./data/"+j2+".txt")):
            f = open("./data/"+j2+".txt","r")
            elo2 = f.readline()
            f.close()
    elif(j2 == "Joueur pair"):
        elo2 = eloJoueurPair
        
    else:
        showinfo("Erreur", "Le joueur en bas n'a pas de fichier !")

    #Calcul du nouveau Elo
    differenceElo = (float)(elo1) - (float)(elo2)
    resultatAttendu1 = (1)/(1+math.pow(10, -differenceElo/400))
    resultatAttendu2 = 1 - resultatAttendu1

    newElo1 = (float)(elo1) + (float)(K) * ((0.5 if egalite else 1) - (float)(resultatAttendu1))
    newElo2 = (float)(elo2) + (float)(K) * ((0.5 if egalite else 0) - (float)(resultatAttendu2))

    newElo1 = math.trunc(newElo1)
    newElo2 = math.trunc(newElo2)

    showinfo("Nouveau elo","L'elo de "+j1+" passe de "+elo1+" � "+(str)(newElo1))
    showinfo("Nouveau elo","L'elo de "+j2+" passe de "+elo2+" � "+(str)(newElo2))

    f = open("./data/"+j1+".txt","w")
    f.write((str)(newElo1))
    f.close()

    f = open("./data/"+j2+".txt","w")
    f.write((str)(newElo2))
    f.close()
    
    matchFenetre.withdraw()
    refreshListeJoueur()



#Root de l'app
fenetre = Tk()
fenetre.geometry("500x500")
#Fin

#Fenetre Principale
mainframe = Frame(fenetre)
mainframe.pack()
label = Label(mainframe, text="Alkemy Elo\n\n")
label.pack()
#Fin


#Menu d�roulant
menubar = Menu(fenetre)


menuDonnee = Menu(menubar, tearoff=0)
menuDonnee.add_command(label="Importer depuis T3 (csv)", command=importT3)
menuDonnee.add_command(label="Ajouter joueur", command=ajouterJoueur)
menuDonnee.add_separator()
menuDonnee.add_command(label="Ajouter points bonus tournoi", command=addBonusPoints)

menuSpecial = Menu(menubar, tearoff=0)
menuSpecial.add_command(label="Parametrer le joueur pair", command=ParamJoueurPair)
menuSpecial.add_command(label="Parametrer le coeff de d�veloppement du elo", command=ParamK)
menuSpecial.add_separator()
menuSpecial.add_command(label="Quitter", command=QuitAll)

menubar.add_cascade(label="Donn�es", menu=menuDonnee)
menubar.add_cascade(label="Sp�cial", menu=menuSpecial)

fenetre.config(menu = menubar)
#Fin

#Frame avec la liste des joueurs
listeJoueurFrame = Frame(mainframe)
labelListeJoueur = Label(listeJoueurFrame, text="Ajouter un tournoi ou des joueurs !")
labelListeJoueur.pack()
buttonMatch = Button(listeJoueurFrame, text="Saisir un match", command=ShowMatchFrame)
buttonMatch.pack()
listeJoueurFrame.pack()
#Fin

#Fenetre de saisie de match
matchFenetre = Tk()
matchFenetre.geometry("250x250")
listeDeroulanteJoueur1 = ttk.Combobox(matchFenetre, values=listeJoueur)
listeDeroulanteJoueur1.current(0)
listeDeroulanteJoueur1.pack()
labelIntercalaire = Label(matchFenetre, text="↑ gagnant | perdant ↓")
labelIntercalaire.pack()
listeDeroulanteJoueur2 = ttk.Combobox(matchFenetre, values=listeJoueur)
listeDeroulanteJoueur2.current(0)
listeDeroulanteJoueur2.pack()
isEgalite = BooleanVar(matchFenetre)
isEgalite.set(False)
checkboxEgalite = Checkbutton(matchFenetre, text="�galit�", variable=isEgalite)
checkboxEgalite.pack()
buttonValider = Button(matchFenetre, text="Valider", command=lambda :validerMatch(listeDeroulanteJoueur1.get(), listeDeroulanteJoueur2.get(), isEgalite.get()))
buttonValider.pack()
buttonAnnuler = Button(matchFenetre, text="Annuler", command=lambda :matchFenetre.withdraw())
buttonAnnuler.pack()
#Fin

matchFenetre.withdraw()
fenetre.mainloop()
