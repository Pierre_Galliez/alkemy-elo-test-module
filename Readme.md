# AlkemyElo Test Module  

## Installation
Il faut installer [python3](https://www.python.org/ftp/python/3.11.1/python-3.11.1-amd64.exe) puis ouvrir un invite de commande :  
```
pip install -r requirements.txt
```

## Usage  
Ouvrir un invite de commande dans le dossier contenant AlkemyElo.py  
Puis : 
```
py AlkemyElo.py
```
Pour fermer l'application ne JAMAIS UTILISER LA CROIX ROIGE mais le bouton "Quitter" dans le menu déroulant "Spécial".  
Mettre le CVS d'export du tournoi dans le dossier /import et rennomer le fichier "import.csv".  
Puis ">Données" et "Importer depuis T3".  

TODO :   
